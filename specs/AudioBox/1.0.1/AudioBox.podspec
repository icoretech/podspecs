#
# Be sure to run `pod lib lint AudioBox.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'AudioBox'
  s.version          = '1.0.1'
  s.summary          = 'AudioBox SDK.'
  s.description      = <<-DESC
                       Official AudioBox SDK for OSX / iOS
                       DESC
  s.homepage         = 'https://github.com/icoretech'
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { 'Claudio Poli' => 'masterkain@gmail.com' }
  s.source           = { git: 'https://bitbucket.org/icoretech/audiobox-apple-framework.git', tag: s.version.to_s }
  s.social_media_url = 'https://twitter.com/icoretech'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'AudioBox' => ['Pod/Assets/*.png']
  }

  s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.framework = %w{libz}
  s.dependency 'AFNetworking', '~> 2.5'
  s.dependency 'AFgzipRequestSerializer', '~> 0.0.2'
end
